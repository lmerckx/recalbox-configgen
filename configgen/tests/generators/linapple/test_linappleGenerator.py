#!/usr/bin/env python
'''
Created on Mar 7, 2016

@author: laurent
'''
import os
import argparse
import cStringIO

# Import runtest classes
import runtest
import linapple_fixture

# Import needed configgen modules
import configgen.emulatorlauncher as emulatorlauncher
import configgen.generators.linapple.linappleGenerator as linappleGenerator
import configgen.generators.linapple.linappleConfig as linConfig
from configgen.generators.linapple.linappleConfig import LinappleConfig
from configgen.generators.linapple.linappleGenerator import LinappleGenerator

class TestLinappleGenerator(runtest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Call base class
        runtest.TestCase.setUpClass('apple2', 'linapple')
        
        # Configure test class resources
        cls.path_init_conf = os.path.join(cls.path_init, 'linapple.conf')
        cls.path_user_conf = os.path.join(cls.path_user, 'linapple.conf')
        linappleGenerator.recalboxFiles.recalboxBins['linapple'] = "/bin/echo"


    @classmethod
    def tearDownClass(cls):
        runtest.TestCase.tearDownClass()
    
    def setUp(self):
        super(self.__class__, self).setUp()
        self.maxDiff = None
    
    def tearDown(self):
        super(self.__class__, self).tearDown()
    
    @runtest.fixture_joystick(linapple_fixture.Joystick, 10)
    def test_main(self):
        # Override LinappleGenerator to echo the command instead than really 
        # executing it.
        generator = LinappleGenerator(self.path_init, self.path_user)
        #generator.cmdArray.insert(0, '/bin/echo')
        emulatorlauncher.lineAppleGeneratorOverride = generator
        
        # Load settings from system configuration file and apply 
        # expected results
        linConfig.setResolutionFile("/nofile")
        config_init = LinappleConfig(self.path_init_conf)
        config_init.settings.update(self.results)

        # Run tested function with updated arguments and load settings from
        # user configuration. 
        self.args.update(self.params)
        self.args["extra"] = ""

        # PATCH
        self.args["nodefaultkeymap"] = False
        self.args["netplay_playerpassword"] = ""
        self.args["netplay_viewerpassword"] = ""
        self.args["netplay_vieweronly"] = False
        for i in range(1, 11):
            p = "p{}".format(i)
            ax = "{}nbaxes".format(p)
            ha = "{}nbhats".format(p)
            bu = "{}nbbuttons".format(p)
            self.args[ax] = "*"
            self.args[ha] = "*"
            self.args[bu] = "*"

        # Call main functions with args updated with fixture parameters and
        # outpout redirected into a string.
        main_stdout = cStringIO.StringIO()
        with runtest.RedirectStdStreams(main_stdout):
            emulatorlauncher.main(argparse.Namespace(**self.args))
        output_str = main_stdout.getvalue()
        main_stdout.close()

        config_user = LinappleConfig(self.path_user_conf)
        
        # Check results
        self.assertDictContentEqual(config_init.settings,
                                    config_user.settings)

    def test_config_upgrade(self):
        generator = LinappleGenerator(self.path_init, self.path_user)
        generator.config_upgrade('v4.0.0 2016/03/20 01:19')
        
    def test_config_upgrade_all(self):
        result = emulatorlauncher.config_upgrade('v4.0.0 2016/03/20 01:19')
        #self.assertTrue(result)

if __name__ == "__main__":
    runtest.main(testLoader=runtest.TestLoader())
    
# Local Variables:
# tab-width:4
# indent-tabs-mode:nil
# End:
# vim: set expandtab tabstop=4 shiftwidth=4: